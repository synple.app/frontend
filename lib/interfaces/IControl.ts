export interface IControl {
  id: string;
  component: string;
  payload: any;
}