export interface IGenerator {
  id?: string;
  name: string;
  code: string;
}